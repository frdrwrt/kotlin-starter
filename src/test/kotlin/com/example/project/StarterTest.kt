package com.example.project

import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

class StarterTest {

    private val starter = Starter()

    @Test
    fun `one should return 1`() {
        assertEquals(1, starter.one())
    }

}
